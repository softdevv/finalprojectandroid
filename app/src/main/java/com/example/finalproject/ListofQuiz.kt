/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.finalproject

import android.graphics.Picture


const val MAX_NO_OF_WORDS = 10
const val SCORE_INCREASE = 20

// List with all the words for the Game
val allWordsList: List<String> =
    listOf("ดาวซัลโวตลอดการของพรีเมียร์ลีกคือใคร",
        "ใครคือใคร",
        "กูคือใคร",
        )

val imgList: List<String> =
    listOf("https://www.google.com/imgres?imgurl=https%3A%2F%2Fpromotions.co.th%2Fwp-content%2Fuploads%2F2022%2F07%2FCristiano-Ronaldo-1.jpg&imgrefurl=https%3A%2F%2Fpromotions.co.th%2Fsports%2Fupdate-on-ronaldos-situation-with-manchester-united.html&tbnid=4h3rAn7BX4EpXM&vet=12ahUKEwjZ0P-BzPv6AhVm83MBHXUVBxAQMygAegUIARCMAQ..i&docid=XCvLPG3uPhvF4M&w=500&h=500&q=%E0%B9%82%E0%B8%A3%E0%B8%99%E0%B8%B1%E0%B8%A5%E0%B9%82%E0%B8%94%E0%B9%89%E0%B8%A5%E0%B9%88%E0%B8%B2%E0%B8%AA%E0%B8%B8%E0%B8%94.jpg&ved=2ahUKEwjZ0P-BzPv6AhVm83MBHXUVBxAQMygAegUIARCMAQ",
    )
