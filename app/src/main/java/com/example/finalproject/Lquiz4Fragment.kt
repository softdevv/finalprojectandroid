package com.example.finalproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.finalproject.databinding.FragmentLquiz3Binding
import com.example.finalproject.databinding.FragmentLquiz4Binding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Lquiz4Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Lquiz4Fragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentLquiz4Binding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLquiz4Binding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnChoice4?.setOnClickListener {
            val action = Lquiz4FragmentDirections.actionLquiz4FragmentToLquiz5Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnChoice2?.setOnClickListener {
            val action = Lquiz4FragmentDirections.actionLquiz4FragmentToLwrong4Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnChoice3?.setOnClickListener {
            val action = Lquiz4FragmentDirections.actionLquiz4FragmentToLwrong4Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnChoice1?.setOnClickListener {
            val action = Lquiz4FragmentDirections.actionLquiz4FragmentToLwrong4Fragment()
            view.findNavController().navigate(action)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Lquiz4Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Lquiz4Fragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}